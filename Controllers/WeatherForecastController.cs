using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using OpenTelemetry.Trace;

namespace sawubona.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;
    private readonly ISomeService _someService;

    public WeatherForecastController(ILogger<WeatherForecastController> logger, ISomeService someService)
    {
        _logger = logger;
        _someService = someService;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<WeatherForecast>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Get()
    {
        try
        {
            var act = Activity.Current;
            act?.SetTag("produktkod", "blaha blaha");

            act?.AddEvent(new ActivityEvent("Ladda sida", tags: new ActivityTagsCollection(
                new[] { new KeyValuePair<string, object?>("EventMedNånNyckel", "12345") })));

            act?.AddEvent(new ActivityEvent("Anropa nån service", tags: new ActivityTagsCollection(
                new[] { new KeyValuePair<string, object?>("EventMedNånNyckel", "123456") })));

            var someResult = _someService.SomeMethod();

            if(DateTime.Now.Second %2 == 0)
                throw new NotImplementedException("testing");

            
            return Ok( Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                    Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    TemperatureC = Random.Shared.Next(-20, 55),
                    Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                })
                .ToArray());
        }
        catch (Exception ex)
        {
            Activity.Current?.SetStatus(ActivityStatusCode.Error, ex.Message);
            Activity.Current?.RecordException(ex);
            return Problem(ex.Message, statusCode: (int)HttpStatusCode.InternalServerError);
        }
    }
}