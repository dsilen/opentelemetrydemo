# Demo app

Skapa upp initial, enkel webapp:

    mkdir sawubona
    cd sawubona
    dotnet new sln
    dotnet new webapi
    dotnet sln add sawubona.csproj
    dotnet new gitignore
    echo .idea >> .gitignore
    git init
    git add -A
    git commit -m "Initial commit"


Kör jaeger med OLTP-importer påslagen, hittade här https://www.jaegertracing.io/docs/1.47/getting-started/:

    docker run -d --name jaeger \
    -e COLLECTOR_ZIPKIN_HOST_PORT=:9411 \
    -e COLLECTOR_OTLP_ENABLED=true \
    -p 6831:6831/udp \
    -p 6832:6832/udp \
    -p 5778:5778 \
    -p 16686:16686 \
    -p 4317:4317 \
    -p 4318:4318 \
    -p 14250:14250 \
    -p 14268:14268 \
    -p 14269:14269 \
    -p 9411:9411 \
    jaegertracing/all-in-one:1.47


Adda nugets:

    <PackageReference Include="OpenTelemetry" Version="1.5.1" />
    <PackageReference Include="OpenTelemetry.Exporter.OpenTelemetryProtocol.Logs" Version="1.5.0-rc.1" />
    <PackageReference Include="OpenTelemetry.Extensions.Hosting" Version="1.5.1" />


Sätt upp opentracing i startupkod:

    builder.Services.AddLogging(l => l.AddOpenTelemetry(t =>
    {
    t.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("practical-otel"));
    t.AddOtlpExporter();
    }));
    
    builder.Services.AddOpenTelemetry()
    .ConfigureResource(b =>
    {
    b.AddService("practical-otel")
    .AddAttributes(new List<KeyValuePair<string, object>>
    { new("sample-key", "sample-value") });
    })
    .WithMetrics(b =>
    {
    b.AddMeter(DiagnosticsConfig.Meter.Name)
    .AddOtlpExporter();
    })
    .WithTracing(t =>
    {
    t.AddOtlpExporter();
    });

