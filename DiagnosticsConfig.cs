using System.Diagnostics;

public static class DiagnosticsConfig
{
    public const string ServiceName = "weatherforecast-service";
    
    public static ActivitySource Source = new(ServiceName, ServiceVersion);
    public const string ServiceVersion = "1.0.0";
}