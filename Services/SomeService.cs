using System.Diagnostics;

public interface ISomeService
{
    string SomeMethod();
}

public class SomeService : ISomeService
{
    public string SomeMethod()
    {
        // Creates a new Span, running the duration of this method
        using var activity = DiagnosticsConfig.Source.StartActivity();

        // Subscope span
        {
            using var subactivity = DiagnosticsConfig.Source.StartActivity(name: "subactivity");
            
            subactivity?.AddEvent(new ActivityEvent("doing some stuff"));
            Thread.Sleep(100);
        }
        
        activity?.AddEvent(new ActivityEvent("doing some more stuff"));
        Thread.Sleep(100);
        
        return "some result";
    }
}