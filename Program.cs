using System.Diagnostics.Metrics;
using OpenTelemetry.Logs;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using sawubona.Controllers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddSingleton<ISomeService, SomeService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddLogging(l => l.AddOpenTelemetry(t =>
{
    t.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(DiagnosticsConfig.ServiceName, serviceVersion: DiagnosticsConfig.ServiceVersion));
    t.AddOtlpExporter();
}));


builder.Services.AddOpenTelemetry()
    .ConfigureResource(b =>
    {
        b.AddService(DiagnosticsConfig.ServiceName, serviceVersion: DiagnosticsConfig.ServiceVersion)
            .AddAttributes(new List<KeyValuePair<string, object>>
            {
                new("machinename", Environment.MachineName ),
                new ("environment", builder.Environment.EnvironmentName)
            });
    })
    .WithTracing(t =>
    {
        t.AddAspNetCoreInstrumentation();  // Instrumentation for ASP.NET Core. Prebuilt statistics things
        t.AddHttpClientInstrumentation();  // Instrumentation for HttpClient. Prebuilt statistics things
        t.AddOtlpExporter();
        t.AddSource(DiagnosticsConfig.Source.Name);
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();